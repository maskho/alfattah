const Banner = (props) => (
  <section id="banner" className="major">
    <div className="inner">
      <header className="major">
        <h1>Bantu Pendanaan Masjid Al Fattah</h1>
      </header>
      <div className="content">
        <p>
          Konsep Masjid Multipurpose: Perpustakaan dan Co-working Space.
          <br />
          Memajukan umat di dunia dan akhirat
        </p>
        <ul className="actions">
          <li>
            <a href="#one" className="button next scrolly">
              Berkenalan Lebih Lanjut
            </a>
          </li>
        </ul>
      </div>
    </div>
  </section>
);

export default Banner;
